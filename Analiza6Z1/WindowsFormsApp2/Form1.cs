﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {
                double y;
                double.TryParse(label2.Text, out y);
                label2.Text = (y + x).ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {
                double y;
                double.TryParse(label2.Text, out y);
                label2.Text = (y - x).ToString();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            { 
                double y;
                double.TryParse(label2.Text, out y);
                if (y == 0) label2.Text = (x).ToString();
                else label2.Text = (y * x).ToString();
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else if (x == 0) MessageBox.Show("Nemozes dijelit s 0 !");
            else
            {
                double y;
                double.TryParse(label2.Text, out y);
                if (y == 0) label2.Text = (x).ToString();
                else label2.Text = (y / x).ToString();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {
                
                label2.Text = (x * x).ToString();
            }

        }

        private void button6_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {

                label2.Text = (x * x * x).ToString();
            }

        }

        private void button7_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else if(x<0) MessageBox.Show("Ne mozes korjenovati negativan broj !");
            else
            {

                label2.Text = (Math.Sqrt(x)).ToString();
            }

        }

        private void button8_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {
                label2.Text = (Math.Sin(x)).ToString();
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(textBox1.Text, out x))
            {
                MessageBox.Show("Unesi broj!");
            }
            else
            {
                label2.Text = (Math.Cos(x)).ToString();
            }

        }

        private void button10_Click(object sender, EventArgs e)
        {
            label2.Text = 0.ToString();
        }
    }
    
}
